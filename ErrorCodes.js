// Error code enumeration
export default class ErrorCodes {
    // Loading data failed. This indicates that some required data cannot be loaded. Usually, this is caused by a
    // missing internet connection. This is also used when invalid configurations are passed.
    static LoadFailed = 1;

    // This is called whenever an unrecoverable error appeared in Inzight. In most cases, this is caused by incompatible
    // hardware. Really rare. Inzight supports WebGL 1 and 2. WebGL 1 is available since 2010/11.
    static Internal = 2;

    // This indicates that content was requested that does not exist.
    static InvalidContent = 3;

    // This indicates that scene models were requested that do not exist.
    static InvalidScene = 4;
}
