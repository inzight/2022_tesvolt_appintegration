// Operation modes enumeration
export default class Modes {
    // Use this mode if you want to show chapters/sections of the interactive manual
    static InteractiveManual = "Manual";

    // Use this mode if you want the user to pick a part in the model
    static PickPart = "Picker";

    // Just show the the 3D models without any additional content or picking.
    static View = "Viewer";
}
