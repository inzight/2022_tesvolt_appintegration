# Inzight App Integration

This is a react-native wrapper around the Inzight components tailored towards Tesvolt.

The Inzight component is a web-app that heavily relies on WebGL. It is provided as a single HTML file that contains
all necessary scripts and assets. To utilize the Inzight component in react-native, this repo provides a wrapper around
a `react-native-webview` and the means to communicate with the Inzight component.

## Pre-Conditions

-   A working node + react-native environment.

## Setup

Install this package and its peer dependencies:

```sh
# Install this component
npm install https://bitbucket.org/inzight/2022_tesvolt_appintegration.git

# Also install the peer dependencies:
npm install react-native-webview

# The component needs a local, static http server. As of this writing,
# the original react-native-static-server is not maintained anymore. A
# working fork can be used (like the one shown here).
#
# It is important to ALIAS the package to "staticHTTP"
npm install staticHTTP@https://github.com/birdofpreyru/react-native-static-server

# For iOS:
cd ios
pod install

```

On Android, make sure to enable AndroidX and Jettifier in `android/gradle.properties`:

```
android.useAndroidX=true
android.enableJetifier=true
```

Also, set the minSdkVersion to 23 in `android/build.gradle`. This is needed for the `react-native-static-server`-fork
used here. If you use another one, this might not be needed.

### Assets

The Inzight component is delivered as a single HTML file that contains all the code, styles and assets required to run
the Inzight engine. During development, we might add more files, though. In [assets/](assets/), you will find all the
files needed. You have to make sure that all the files in the [assets](assets/) directory are available during runtime.
It does not matter how you do it and where you put them. You have to provide the path to the Inzight react components.
This directory has to be readable. Write-access is not required.

The [demo](demo/) shows a simple asset setup for reference. The file [demo/InzightAssets.js](demo/InzightAssets.js)
shows an example implementation. The [Demo Readme](demo/README.md#AssetSetup) explains the required steps to set this up.

## Usage

The provided component is a regular react-component. You can use it as such. It takes some parameters and provides
results via callback. A detailed and fully documented example can be found in [demo/InzightComponents.js](demo/InzightComponents.js).

A simple example:

```js
// These are the components that wrap INZIGHT
import { View as InzightView } from "inzight-tesvolt-appintegration";
import { Modes, ErrorCodes, ResultCodes } from "inzight-tesvolt-appintegration";

// Use the component in your own "InstallationManual"-component:
export const InstallationManual = ({}) => {
    // The path pointing to the Inzight assets. Asset management is
    // comletely up to you.
    //  - refer to demo/InzightAssets.js for a demo implementation.
    const path = assetPath();

    // A configuration is required.
    const config = {
        // This privdes the configuration options needed to show manuals, enable
        // parts-picking and more.

        // Exemplary
        //  - refer to demo/InzightComponents.js for a very detailled
        //    explanation of all configuration options.

        language: "en",
        contentSequence: [
            {
                chapter: "Installation",
                section: "Grounding",
            },
        ],
        scene: [
            {
                // Part ID of the rack
                rack: "101021",
                // Part ID of the APU
                apu: "100659",
                // The amount of batteries. 1 is minimum.
                numBat: 6,
            },
        ],
        mode: Modes.InteractiveManual,
    };

    return (
        <InzightView
            path={path}
            configuration={config}
            onError={(code) => {
                console.error(code);
            }}
            onResult={(code, result) => {
                console.log(code, result);
            }}
        />
    );
};
```

## Available Parts:

A scene has to be constructed from one rack and a set of modules. The available models and their part numbers are listed
here.

| Part ID | Tesvolt Name                      | Internal Name | Type   |
| ------- | --------------------------------- | ------------- | ------ |
| 103509  | Batteriemodul 4.8-1C-LV48_BTR     | bat           | module |
| 102705  | Batterie LV                       |               | module |
| 100298  | Batterie LV                       |               | module |
| 100659  | APU LV                            | apu           | module |
| 104069  | APU LV (BTR)                      |               | module |
| 101024  | Batterieschrank TS 25 (VX System) | vx25          | rack   |
| 101021  | Batterieschrank TS 40 (VX System) | vx40          | rack   |
| 101022  | Batterieschrank TS 50 (VX System) | vx50          | rack   |

## Available Manuals/Sections:

Manual: `InstallationUndBetriebTS48V`

| Tesvolt Manual                                                | Inzight Section Code                                                              |
| ------------------------------------------------------------- | --------------------------------------------------------------------------------- |
| Chapter 6.1, Step 10 - 16 (VX Racks) + Chapter 6.3, Step 1- 8 | `installation/installation der komponenten/montage der module`                    |
| Chapter 6.3, Step 9 – 14                                      | `installation/installation der komponenten/montage dc verbinder`                  |
| Chapter 6.3, Step 19                                          | `installation/installation der komponenten/sperrzahnmutter festziehen`            |
| Chapter 6.3, Step 20                                          | `installation/installation der komponenten/seitliche abdeckung`                   |
| Chapter 6.3, Step 21 – 22                                     | `installation/installation der komponenten/batfuse verbinden`                     |
| Chapter 6.3, Step 18                                          | `installation/installation der komponenten/batcom verbinden`                      |
| Chapter 6.3, Step 17                                          | `installation/installation der komponenten/rack balancing stecken`                |
| Chapter 6.3, Step 23 -24                                      | `installation/installation der komponenten/apu can sma stecken`                   |
| Chapter8.1, Step 6                                            | `inbetriebnahme/inbetriebnahme eines einzelnen geraets/batterianzahl bestaetigen` |
| Chapter8.1, Step 7                                            | `inbetriebnahme/inbetriebnahme eines einzelnen geraets/start apu pruefen`         |
| Chapter8.1, Step 7                                            | `inbetriebnahme/inbetriebnahme eines einzelnen geraets/start apu pruefen`         |
| Chapter8.1, Step 8                                            | `inbetriebnahme/inbetriebnahme eines einzelnen geraets/lan verbindung herstellen` |

## Offline Mode

Work in Progress - Refer to SAPP-23.

## Example

A fully documented example can be found in [demo](demo/).

## Known Issues

-   The UI **IN** the Inzight Scene is disabled. We are updating it to be more responsive. Refer to SAPP-26.
-   `onError` and `onResult` are not yet used. Refer to SAPP-25.
-   Download API not yet defined. Refer to SAPP-23.
-   Wrapping the Inzight component in a <View> crashes on Android if the style is not "flex"
    -   Refer to this [issue](https://github.com/react-native-webview/react-native-webview/issues/811) on Github.
