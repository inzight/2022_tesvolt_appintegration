// Result code enumeration
export default class ResultCodes {
    // The user confirmed a selection.
    static OK = 1;
    // The user aborted the operation.
    static Abort = 2;
}
