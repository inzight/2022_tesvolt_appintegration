import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";

import StaticServer from "staticHTTP";
import { WebView } from "react-native-webview";

import { ErrorCodes } from "inzight-tesvolt-appintegration";

export default class Inzight extends Component {
    state = {
        url: null,
    };

    constructor(props) {
        super(props);

        if (props.path == null) {
            throw "Missing property: 'path'";
        }
        if (props.configuration == null) {
            throw "Missing property: 'configuration'";
        }

        this.m_path = props.path;
        this.m_configuration = props.configuration;
        this.m_onError = props.onError;
        this.m_onResult = props.onResult;
    }

    componentDidMount() {
        this.m_server = new StaticServer(8080, this.m_path);
        this.m_server.start().then((url) => {
            this.setState({ url });
        });
    }

    componentWillUnmount() {
        if (this.m_server && this.m_server.isRunning()) {
            this.m_server.stop();
        }
    }

    onMessage(event) {
        const res = JSON.parse(event.nativeEvent.data);
        if(res.type == "error")
        {
            // Map the internal categories to the ones listed in ErrorCodes.js
            const mapError = (category)=>{
                switch(category){
                    case 0: // Internal
                        return ErrorCodes.Internal;
                    case 1: // Load
                        return ErrorCodes.LoadFailed;
                    case 2: // Content
                        return ErrorCodes.InvalidContent;
                    default: // handle everything else as internal error.
                        return ErrorCodes.Internal;
                }
            }

            if (this.m_onError && typeof this.m_onError === typeof Function) {
                this.m_onError(mapError(res.category), res);
            }
        }
        else {
            if (this.m_onResult && typeof this.m_onResult === typeof Function) {
                this.m_onResult(ResultCodes.OK, res);
            }
        }
    }

    configInjector() {
        // NOTE: the lone "true;" at the end helps circumventing a long standing bug on iOS where the code is not
        // injected properly.
        // https://github.com/react-native-webview/react-native-webview/issues/1291
        return 'var InjectedConfiguration = \'' + JSON.stringify(this.m_configuration) + '\';window.InjectedConfiguration=InjectedConfiguration;true;';
    }

    render() {
        const styles = StyleSheet.create({
            containerWithoutGutter: {
                // CRITICAL: the view that wraps the component needs to be styled as flex.
                // Refer to https://github.com/react-native-webview/react-native-webview/issues/811
                flex: 1,
                maxHeight: "100%",
                marginTop: 0,
                marginHorizontal: 0,
            },
        });

        let content;
        if (!this.state.url) {
            content = <Text></Text>;
        } else {
            content = (
                <WebView
                    injectedJavaScriptBeforeContentLoaded={this.configInjector()}
                    onMessage={this.onMessage.bind(this)}
                    originWhitelist={["*"]}
                    source={{ uri: this.state.url + "/inzight.html" }}
                    showsHorizontalScrollIndicator={false}
                    bounces={false}
                    scrollEnabled={false}
                    javaScriptEnabledAndroid={true}
                    mediaPlaybackRequiresUserAction={false}
                    cacheEnabled={true}
                />
            );
        }

        // Wrapping WebView in a <View> causes crashes on Android. It must be styled "flex".
        //  - https://github.com/react-native-webview/react-native-webview/issues/811
        return <View style={styles.containerWithoutGutter}>{content}</View>;
    }
}
