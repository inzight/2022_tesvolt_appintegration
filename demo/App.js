import React from "react";
import { StyleSheet, Button, View, ScrollView, SafeAreaView, Text } from "react-native";

import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

import { InstallationManual, PartsPicker, PartsViewer } from "./InzightComponents";
import { assetSetup } from "./InzightAssets";

const Stack = createNativeStackNavigator();

const App = () => {
    assetSetup();

    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name="Home" component={HomeScreen} options={{ title: "INZIGHT Integration Demo" }} />
                <Stack.Screen name="InstallationManual" component={InstallationManual} />
                <Stack.Screen name="PartsPicker" component={PartsPicker} />
                <Stack.Screen name="PartsViewer" component={PartsViewer} />
            </Stack.Navigator>
        </NavigationContainer>
    );
};

const Separator = () => <View style={styles.separator} />;

const HomeScreen = ({ navigation }) => {
    return (
        <SafeAreaView style={styles.container}>
            <ScrollView contentInsetAdjustmentBehavior="automatic">
                <View>
                    <Text style={styles.sectionTitle}>INZIGHT Installation Manual</Text>
                    <Text style={styles.sectionDescription}>
                        This opens a react-native webview to show the 3D installation manual.
                    </Text>
                    <Button title="Show Manual" onPress={() => navigation.navigate("InstallationManual", {})} />
                </View>
                <Separator />
                <View>
                    <Text style={styles.sectionTitle}>INZIGHT Parts</Text>
                    <Text style={styles.sectionDescription}>This opens the parts picker.</Text>
                    <Button title="Show Parts Picker" onPress={() => navigation.navigate("PartsPicker", {})} />
                </View>
                <Separator />
                <View>
                    <Text style={styles.sectionTitle}>INZIGHT Parts Viewer</Text>
                    <Text style={styles.sectionDescription}>This opens the parts viewer.</Text>
                    <Button title="Show Parts Viewer" onPress={() => navigation.navigate("PartsViewer", {})} />
                </View>
            </ScrollView>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    sectionTitle: {
        color: "#444",
        fontSize: 24,
        fontWeight: "600",
    },

    sectionDescription: {
        color: "#000",
        marginTop: 8,
        marginBottom: 8,
        fontSize: 18,
        fontWeight: "400",
    },

    container: {
        marginTop: 8,
        marginHorizontal: 16,
    },

    separator: {
        marginVertical: 16,
        borderBottomColor: "#737373",
        borderBottomWidth: StyleSheet.hairlineWidth,
    },
});

export default App;
