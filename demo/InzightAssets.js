///////////////////////////////////////////////////////////////////////////////
// This is some trivial code for finding the assets required for INZIGHT.
//
// The inzight component requires some assets to be present in the app. It is
// up to you to make it appear in some directory that can be read during runtime.
// Use whatever tools you like to handle the assets.
//
// In this an example, the bundle path (ios) and document path (android) are used.
// On android, the provided asset is copied to the document paths on startup in
// App.js.
//
// The location where the files are in will be returned by assetPath and passed
// into the Inzight components. The Inzight components never write to this. It
// is only used for reading the assets.
//

import RNFS from "react-native-fs";

// The list of files.
const inzightAssets = ["inzight.html"];

// Move the assets from the package to the document dir of the app.
export const assetSetup = async () => {
    if (Platform.OS === "android") {
        const p = assetPath();
        await RNFS.mkdir(p);
        inzightAssets.forEach(async (file) => {
            await RNFS.copyFileAssets(file, p + "/" + file);
        });
    }
};

// The path where the assets can be found at runtime.
export const assetPath = () => {
    return Platform.OS === "android"
        // This is set in android/app/build.gradle
        ? RNFS.DocumentDirectoryPath + "/InzightAssets"
        // The assets folder was linked in Xcode
        : RNFS.MainBundlePath + "/assets";
};

// end asset management
///////////////////////////////////////////////////////////////////////////////
