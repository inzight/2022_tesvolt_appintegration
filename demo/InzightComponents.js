import React from "react";
import { Alert, View } from "react-native";

// These are the components that wrap INZIGHT
import { View as InzightView } from "inzight-tesvolt-appintegration";
import { Modes, ErrorCodes, ResultCodes } from "inzight-tesvolt-appintegration";

// The assets helper
import { assetPath } from "./InzightAssets";

///////////////////////////////////////////////////////////////////////////////
// These components provide a copy&paste blueprint.
//

// The error-handler. The inzight component defines different error codes.
// Handle them here.
const onError = (code, details) => {
    console.error(details);

    // An error code is provided. Check its meaning and provide some messages:
    switch (code) {
        case ErrorCodes.LoadFailed:
            Alert.alert("Error", "Loading data failed. Are you online?");
            break;
        case ErrorCodes.Internal:
            Alert.alert("Error", "Internal error. Sorry :-(");
            break;
        case ErrorCodes.InvalidContent:
            Alert.alert("Error", "The requested content was not found.");
            break;
        case ErrorCodes.InvalidScene:
            Alert.alert("Error", "The requested Tesvolt device setup was not found.");
            break;
    }
};

// The installation manual as a component
export const InstallationManual = ({}) => {
    // We need to know where the Inzight assets are.
    const path = assetPath();

    // The View provided by Inzight is generic. It renders whatever you request
    // it to render.
    // -> Define a configuration:
    const config = {
        // First, define the language to use. Use ISO-639-1 encoding.
        language: "en", // or "de", "es", ...

        // The Inzight component can be seen as a book. A book contains
        // chapters on different topics, each chapter contains sections, and so
        // on.
        content: {
            manual: "InstallationUndBetriebTS48V",
            section: "installation/installation der komponenten/montage der module",
        },

        // Here, you define the configuration of the Tesvolt Rack+Modules the
        // user has bought. This list basically defines the things the user
        // sees on screen. The official tesvolt parts numbers are used here.
        scene: [
            {
                // Part ID of the rack
                rack: "101024",
                // Part ID of the APU
                apu: "100659",
                // The amount of batteries. 1 is minimum.
                numBat: 3,
            },
        ],

        // Define a mode - Inzight can show an interactive manual or allows to
        // pick a part. In this example component, the user sees the interactive
        // manual at the given chapters.
        mode: Modes.InteractiveManual,

        // We will extend this configuratiuon structure on demand.
        // ...
    };

    // Inzight is nothing more than a component
    return (
        // Wrapping WebView in a <View> (or SafeAreaView) causes crashes on Android. It must be styled "flex".
        //  - https://github.com/react-native-webview/react-native-webview/issues/811
        <View style={{ flex: 1 }}>
            <InzightView
                // Specify the configuration
                configuration={config}
                // The path where to find the Inzight assets
                path={path}
                // An (optional) error callback:
                onError={onError}
            />
        </View>
    );
};

// The parts picking a component. It is mostly the same as above. Check
// `InstallationManual` for reference.
export const PartsPicker = ({}) => {
    const path = assetPath();

    // The parts picker also needs a configuration. Please check the config in
    // InstallationManual for further details.

    const config = {
        language: "en", // or "de", "es", ...

        // A content specification is not needed as we do not show any. Only
        // the parts are shown for picking by the user.

        // As in InstallationManual, you need to define the parts of the user
        scene: [
            {
                // Part ID of the rack
                rack: "101021",
                // Part ID of the APU
                apu: "100659",
                // The amount of batteries. 1 is minimum.
                numBat: 6,
            },
        ],

        // Set the mode accordingly.at the given chapters.
        mode: Modes.PickPart,
    };

    return (
        // Wrapping WebView in a <View> (or SafeAreaView) causes crashes on Android. It must be styled "flex".
        //  - https://github.com/react-native-webview/react-native-webview/issues/811
        <View style={{ flex: 1 }}>
            <InzightView
                path={path}
                configuration={config}
                onError={onError}
                // In contrast to the interactive manual, the parts picker should return the parts number if the user
                // selected any when exiting. The result is a string containing the parts number.
                onResult={(code, result) => {
                    switch (code) {
                        case ResultCodes.OK:
                            Alert.alert("Pick Result", "User picked: " + JSON.stringify(result));
                            break;
                        case ResultCodes.Abort:
                            Alert.alert("Pick Result", "User cancled");
                            break;
                    }
                }}
            />
        </View>
    );
};

// The plain viewer. Use this to view a part without any further content.
export const PartsViewer = ({}) => {
    const path = assetPath();

    // The parts viewer also needs a configuration. Please check the config in
    // InstallationManual for further details.

    const config = {
        language: "en", // or "de", "es", ...

        // A content specification is not needed as we do not show any. Only
        // the parts are shown.

        // As in InstallationManual, you need to define the parts of the user
        scene: [
            {
                // Part ID of the rack
                rack: "101024",
                // Part ID of the APU
                apu: "100659",
                // The amount of batteries. 1 is minimum.
                numBat: 5,
            },
            {
                // Part ID of the rack
                rack: "101021",
                // Part ID of the APU
                apu: "100659",
                // The amount of batteries. 1 is minimum.
                numBat: 2,
            },
        ],

        // Set the mode accordingly.at the given chapters.
        mode: Modes.View,
    };

    return (
        // Wrapping WebView in a <View> (or SafeAreaView) causes crashes on Android. It must be styled "flex".
        //  - https://github.com/react-native-webview/react-native-webview/issues/811
        <View style={{ flex: 1 }}>
            <InzightView path={path} configuration={config} onError={onError} />
        </View>
    );
};

// end Inzight
///////////////////////////////////////////////////////////////////////////////
