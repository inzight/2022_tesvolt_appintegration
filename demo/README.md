# Inzight App Integration Demo

A simple demo app that utilizes the Inzight WebGL component.

## Building and Running

You **have to use yarn**. When using _npm_, the Inzight component will be linked to node-modules. This is not compatible
with the metro bundler. An alternative would be to use [Haul](https://github.com/callstack/haul) as a bundler.

```sh
# Install packages
yarn

# For iOS:
cd ios && pod install

# Run the bundler
yarn run start

# Start the app. Ensure a device is connected or the emaulator is setup.
# This also copies the assets to the right directories.
yarn run android
yarn run ios
```

## Initial Setup

**For later reference only. NOT needed to build the demo app.**

This section describes how this demo was created. Do not follow these steps if you want to build and run the demo.

First, create the app skeleton:

```sh
npx react-native init demo
cd demo

# For some simple navigation accross screens:
npm install @react-navigation/native @react-navigation/native-stack
npm install react-native-screens react-native-safe-area-context

# To find and handle the assets
npm install react-native-fs

# Also install the peer dependencies inzight-tesvolt-appintegration
npm install react-native-webview

# The inzight component requires a static http server. It is aliased in its peerDependencies.
# The current, official react-native-static-server os not working anymore. This one is maintained and working. Install
# to satisfy the staticHTTP alias:
npm install staticHTTP@https://github.com/birdofpreyru/react-native-static-server/

# Link the inzight-tesvolt-appintegration package. Links will cause trouble. See below.
npm install ..
```

Do target OS specific modifications:

-   For iOS, remember to do `cd ios && pod install`.
-   For Android:
    -   Enable AndroidX and Jettifier in `android/gradle.properties`:
        ```
        android.useAndroidX=true
        android.enableJetifier=true
        ```
    -   Set the minSdkVersion to 23 in `android/build.gradle`. This is needed for the `react-native-static-server` to build correctly.

**IMPORTANT:** If the metro bundler complains about missing "inzight" packages, remove the node-modules and install
everything again, using yarn. Yarn does not link the inzight package. It copies it. Metro is not able to handle
symlinks.

### Assets {#AssetSetup}

The `inzight-tesvolt-appintegration` package provides assets that are required. We need to provide them in the app
bundle.

For Android, edit `android/app/build.gradle` and add:
```java
android {
    // ...

    // This copies the assets from the inzight package to the apk.
    sourceSets { main { assets.srcDirs = ['src/main/assets', '../../node_modules/inzight-tesvolt-appintegration/assets/'] } }
}
```

For iOS, you have to add the assets using XCode. Open the project and right-click on "InzightIntegrationDemo". Choose "Add files to InzightIntegrationDemo". Select the assets folder as a whole.
